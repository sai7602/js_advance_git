// 'use strict';



let imageArray1 = [
				
  "./img/cat1.jpg",		
  "./img/cat2.jpg",		
  "./img/cat3.jpg",		
  "./img/cat4.jpg",		
  "./img/cat5.jpg",		
  "./img/cat6.jpg",		
  "./img/cat7.jpg",		
  "./img/cat8.jpg",		
];
let imageArray2 = [
				
  "./img/cat1.jpg",		
  "./img/cat2.jpg",		
  "./img/cat3.jpg",		
  "./img/cat4.jpg",		
  "./img/cat5.jpg",		
 		
];
  class multiItemSlider {
    constructor (array, interval, isCycling, direction, target) {

          // Створення блоку картинок  

        this.mainElement = document.createElement('div');// основний элемент блоку
        this.mainElement.className = "slider";
        this.mainElementWraper = document.createElement('div');// врапер блоку
        this.mainElementWraper.className = "slider__wrapper small__slider__wrapper";
        this.array = array;
        this.isCycling = isCycling;
        this.interval = interval;
        this.direction = direction;
        this.target = document.getElementById(target);
        this.bigBlockWrapper = document.getElementById('big__block__wrapper');
        this.mouseEvent();
        this.createButton();
        this.createBlock();
        this.imageBlock();
        this.autoSlideRun(this.direction, this.interval);
    }
    autoSlideRun(_direction, _interval){
      if (this.isCycling){
        this.intervals = setInterval (() => { 
          this.slider(_direction);
        }, _interval);
      } return;
    }

    slider (direction){
      let position = this.mainElement.querySelectorAll('.slider__item');
      if (direction === 'right'){
        position.forEach(element => {
          let indx = element.style.order;
          if (indx == position.length-1) {
            indx = 0;
            element.style.order = indx;
          } else {
            indx ++;
            element.style.order = indx;
          }
        });
      } else {
        position.forEach(element => {
          let indx = element.style.order;
          if (indx == 0) {
            indx = position.length-1;
            element.style.order = indx;
          } else {
            indx --;
            element.style.order = indx;
          }
        });
      }
    } 

    mouseEvent(){
      this.mainElementWraper.addEventListener('mouseenter', () => {
        clearInterval(this.intervals)
      }); // Очистка автопрокрутки
      this.mainElementWraper.addEventListener('mouseleave', (e) => { 
        if (e.target.classList.contains('small__slider__wrapper')){
          clearInterval(this.intervals);
          this.autoSlideRun(this.direction, this.interval); 
        }
      }); // Додавання автопрокрутки
      
    }

    imageBigSmall(size){





    }

    imageBlock(){ // Блок створення збільшенної картинки
    let item = this.mainElement.querySelectorAll('.slider__item');
    this.bigBlockWrapper.addEventListener('click', ()=> {
      this.bigBlockWrapper.style.display = 'none';
      clearInterval(this.intervals);
      this.autoSlideRun(this.direction, this.interval);

      item.forEach(elem => {
        this.target.append(this.mainElement)
      if (elem.classList.contains('big__slider__item')) {
        elem.classList.toggle('big__slider__item');
        elem.classList.toggle('small__slider__item');
        elem.parentElement.classList.toggle('big__image__wrapper');
        elem.parentElement.classList.toggle('small__slider__wrapper');
        item.forEach(items => {
          if (items.style.order !== elem.style.order ){
            items.classList.toggle('big__slider__item');
            items.classList.toggle('small__slider__item');
            let num = elem.style.order;
            if (items.style.order-num < 0 ) {
              items.style.order = items.style.order - num + item.length;
            } else if (items.style.order-num > 0 ){
              let orderNew =  items.style.order - num;
              items.style.order = orderNew;
            }
          } 
        });
        elem.style.order = 0

        if ((elem.style.order) !==0 ){
          item.forEach(element => {
            let num = elem.style.order;
            if (element.style.order-num < 0 ) {
              element.style.order = element.style.order - num + element.length;
            }
          })
        }

      }
      })

    } )

      item.forEach(elem => {
        elem.addEventListener('click', () => {
     
          if (elem.classList.contains('small__slider__item')) {
            document.getElementById('big__block').append(this.mainElement)
            this.bigBlockWrapper.style.display = 'block';
            clearInterval(this.intervals);
          elem.classList.toggle('big__slider__item');
          elem.classList.toggle('small__slider__item');
          elem.parentElement.classList.toggle('big__image__wrapper');
          elem.parentElement.classList.toggle('small__slider__wrapper');
          item.forEach(items => {
            if (items.style.order !== elem.style.order ){
              items.classList.toggle('big__slider__item');
              items.classList.toggle('small__slider__item');
              let num = elem.style.order;
              if (items.style.order-num < 0 ) {
                items.style.order = items.style.order - num + item.length;
              } else if (items.style.order-num > 0 ){
                let orderNew =  items.style.order - num;
                items.style.order = orderNew;
              }
            } 
          });
          elem.style.order = 0

          if ((elem.style.order) !==0 ){
            item.forEach(element => {
              let num = elem.style.order;
              if (element.style.order-num < 0 ) {
                element.style.order = element.style.order - num + element.length;
              }
            })
          }
        };
        })
      });

        clearInterval(this.interval);
  } 

 
    // Блок створення картинок -- Старт
    createBlock(){
        this.array.forEach((element, id) => {
        this.sliderItems = document.createElement('div'); // додавання элементів (.slider-item)
        this.sliderItems.className = `slider__item small__slider__item`;
        this.sliderItems.innerHTML +=  
        `<img src="${element}" alt="Тут має бути картинка">`
          this.sliderItems.style.order = id;
        this.mainElementWraper.append(this.sliderItems); // Додавання у врапер
        
      });
      this.mainElement.append(this.mainElementWraper); // Додавання у основний блок
}

    
  // Блок створення картинок -- Кінець
   

  // Додавання кнонок -- Старт
    createButton(){
    this.mainElement.innerHTML += `<button class="slider__control slider__control_left"></button>
    <button class="slider__control slider__control_right"></button>`;
    this.mainElement.querySelector('.slider__control_left').addEventListener('click', () => {
      this.slider('right');
    });
    this.mainElement.querySelector('.slider__control_right').addEventListener('click', () => {
      this.slider();
    }
      );
      
      this.target.append(this.mainElement)
  }
  // Додавання кнопок -- Кінець

}
document.addEventListener('DOMContentLoaded', ()=>{
let test1 = new multiItemSlider(imageArray1, 2000, true, 'right', 'first__block');
let test2 = new multiItemSlider(imageArray2, 2000, true, 'left', 'second__block');

});
