const page = document.getElementById('page');
const menu = document.getElementById('menu');

/*
  События касаний:
    touchstart
    touchmove
    touchend
    touchcancel

    Необходимые свойства для работы с касаниями:
    page.addEventListener('touchstart', (e) => {

      e.touches - Массив в котоом хранятся все касания
      https://developer.mozilla.org/ru/docs/Web/API/TouchEvent/touches

      e.changedTouches - Массив касаний, в котором в зависимости от события
      касания находятся те или иные данные касательно касаний.
      https://developer.mozilla.org/ru/docs/Web/API/TouchEvent/changedTouches
    });

*/

// Записываем стартовую позицию
let startPos;
// Записываем текущую позицию при движении
let currentPos;

// Начало касания
page.addEventListener('touchstart', (e) => {
  // console.log('touchstart', e, e.touches[0].clientX);
  // Фиксируем, с какой точки у нас началось движение
  startPos = e.touches[0].clientX;
  // Убираем анимацию на старте меню
  menu.style.transition = '0s';
});

// Движение пальца
page.addEventListener('touchmove', (e) => {
  // Отмена действия нужна, что бы не срабатывал нативный скролл
  e.preventDefault();
  // console.log('touchmove', e, e.touches[0].clientX);

  /*
    Переменные для просчета
      - touchPos - текущая позиция касания
      - change - изменения от начала касания до текущего момента
      - threshold - ширина экрана для свайпа
  */
  let touchPos = e.touches[0].clientX;
  let change = startPos - touchPos;
  let threshold = screen.width / 3;

  // Определяем направление касания - старт > touchPos
  if( startPos > touchPos ){
  // // Если наша позиция меньше начальной - значи движение идет влево
    if( currentPos < touchPos ){
      // Если текщая позиция меньше предыдущей - значит движение было изменено
      console.log('< then >');
    } else {
      console.log('<');
    }
  //   // обновляем текущее положение для просчета изменений движения
    currentPos = touchPos;
  } else {
  //   // 7. Если наша позиция больше начальной - значи движение идет вправо
    if( currentPos > touchPos){
      console.log('> then <');

        page.style.left = `${change * -1}px`;
        menu.style.left = `-${ screen.width - (change * -1)}px`;

        if( (change * -1) > threshold  ){
          page.classList.add('move');
        } else {
          page.classList.remove('move');
        }

    } else {
      console.log('>');

      if( change < 0){
  //       // при движении вправо, change будет уходить в минус
  //       // нужно преобразовать измененое движение в пиксели сделать из -150 => left: 150px;
        page.style.left = `${change * -1}px`;
  //       // показываем меню
        menu.style.display = 'flex';
  //       // ( 375 - (-x * -1 ) ) => 375 - 250 => left : 125px
        menu.style.left = `-${ screen.width - (change * -1)}px`;
      }
  //     // Если наших изменений достаточно для прокрутки подсветим зеленым
      if( (change * -1) > threshold  ){
        page.classList.add('move');
      }
    }
  //   // обновляем текущее положение для просчета изменений движения
    currentPos = touchPos;
  }
});

// Конец касания
page.addEventListener('touchend', (e) => {
  // console.log('touchend', e);
  /*
    - endPos - Конечная позиция касания
    - change - Конечное изменение
    - threshold - Ширина экрана для свайпа
  */
  let endPos = e.changedTouches[0].clientX;
  let change = startPos - endPos;
  let threshold = screen.width / 3;

  // После окончания движения переменная с прошлой позицией не нужна - сбрасываем её
  currentPos = null;

  // Был ли совершен свайп
  let swipe = (change*-1) > threshold ;
  console.log('IS SWIPE?:', change*-1, swipe, threshold);

  if( swipe ){
  //   // Если свайп был - додвигаем наш стартовый экран вправо, до упора с анимацией
    page.style.left = '100%';
    page.style.transition = '0.3s';
  //   // Ставим менюшку на 0px по X
    menu.style.left = `0`;
    menu.style.transition = '0.3s';
  } else {
    // Если не хватило для свайпа сбрасываем все на исходную
    page.classList.remove('move');
    page.style.left = 0;
    menu.style.left = `-100%`;
    // menu.style.transition = '0.3s';
  }

})
