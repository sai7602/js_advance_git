

    /*

        Документация:
        
        https://developer.mozilla.org/ru/docs/HTML/HTML5/Constraint_validation
        
        form.checkValidity() > Проверка всех полей формы на валидость
        form.reportValidity() > Проверяет все поля на валидность и выводит возле каждого из не прошедшего валидацию
            сообщение с ошибкой

        formElement.validity > Объект с параметрами валидности поля 
        formElement.setCustomValidity(message) > Метод который задаст validity.valid = false, и при попытке отправки
            сообщения выведет message в браузерный попал

        Классы для стилизации состояния элемента
        input:valid{}
        input:invalid{}

        
        Задание:
        Используя браузерное API для валидации форм реализовать валидацию следующей формы:

        - Имя пользователя: type:text -> validation: required; minlength = 2;  
            Если пустое выввести сообщение: "Как тебя зовут дружище?!"
        - Email: type: email -> validation: required; minlength = 3; validEmail;
            Если эмейл не валидный вывести сообщение "Ну и зря, не получишь бандероль с яблоками!"
        - Пароль: type: password -> validation: required; minlength = 8; maxlength=16;
            Если пустой вывести сообщение: "Я никому не скажу наш секрет";
        - Количество сьеденых яблок: type: number -> validation: required; minlength = 1; validNumber;
            Если количество 0 вывести эррор с сообщением "Ну хоть покушай немного... Яблочки вкусные"
        - Напиши спасибо за яблоки: type: text -> validation: required; 
            Если текст !== "спасибо" вывести эррор с сообщением "Фу, неблагодарный(-ая)!" используя setCustomValidity();

        - Согласен на обучение: type: checkbox -> validation: required;
            Если не выбран вывести эррор с сообщение: "Необразованные живут дольше! Хорошо подумай!"

        Внизу две кнопки:

        1) Обычный submit который отправит форму, если она валидна.
        2) Кнопка Validate(Проверить) которая запускает методы:
            - yourForm.checkValidity: и выводит на страницу сообщение с результатом проверки
            - yourForm.reportValidity: вызывает проверку всех правил и вывод сообщения с ошибкой, если такая есть

    */
   document.addEventListener("DOMContentLoaded", function () {
       let inputArr = document.querySelector('form');
       let text = inputArr.elements.text;
       let email = inputArr.elements.email;
       let password = inputArr.elements.password;
       let number = inputArr.elements.number;
       let apple = inputArr.elements.apple;
       let checkbox = inputArr.elements.checkbox;
       let valRezalt = inputArr.elements.ValidateResult;
       console.dir(valRezalt)

        function Valid (obj, prop, string){
            var valProp = obj.validity[prop]
            console.log(valProp)
            if (valProp){
                obj.setCustomValidity(string);
                } else {
                    obj.setCustomValidity ('')  
                }
        }

    text.addEventListener("input", function (e) {
        Valid (e.target, "tooShort", 'Как тебя зовут дружище?!')
    });
    email.addEventListener("input", function (e) { 
        Valid (e.target, "typeMismatch", 'Ну и зря, не получишь бандероль с яблоками!')
    });
    password.addEventListener("input", function (e) { 
        Valid (e.target, "tooShort", 'Я никому не скажу наш секрет');
    });
    number.addEventListener("input", function (e) { 
        Valid (e.target, "rangeUnderflow", 'Ну хоть покушай немного... Яблочки вкусные')
    });
    apple.addEventListener("input", function (e) { 
       if (apple.value !== "спасибо"){
        apple.setCustomValidity('Фу, неблагодарный(-ая)!')
        } else {
            apple.setCustomValidity ('')
        }
    });
    checkbox.setCustomValidity('Необразованные живут дольше! Хорошо подумай!')
    checkbox.addEventListener("change", function (e) { 
    if (checkbox.checked){
        checkbox.setCustomValidity ('')
    } else {
        checkbox.setCustomValidity('Необразованные живут дольше! Хорошо подумай!')
     }
    });
    var tt = document.createElement('ul');
    valRezalt.addEventListener('click', function (){
        tt.innerHTML = null;
        document.querySelectorAll('input').forEach(function(e){
            if (!e.validity.valid) {
                var list = document.createElement('li');
                list.innerHTML =` Невірно введні дані в поле  ${e.name} `
                tt.append(list);
            } 
            inputArr.append(tt);

        })
    });
       
  
   });