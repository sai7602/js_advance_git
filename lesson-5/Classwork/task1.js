/*

    Задание 1:

    Написать обьект Train у которого будут свойства:
    -имя,
    -скорость езды
    -количество пассажиров
    Методы:
    Ехать -> Поезд {name} везет { количество пассажиров} со скоростью {speed}
    Стоять -> Поезд {name} остановился. Скорость {speed}
    Подобрать пассажиров -> Увеличивает кол-во пассажиров на Х
*/
document.addEventListener("DOMContentLoaded", function () {
    var Train = {
        name : '709',
        speed : 50,
        passangers: 10,
        ride : function (){
            console.log ( 'Потяг ' + this.name + ' перевозить ' + this.passangers + ' пасажирів зі швидкістю ' + this.speed + ' км/годину')
        },
        stop: function (){
            this.speed = 0;
            console.log ( 'Потяг ' + this.name + ' Зупинився. Швидкість ' + this.speed + ' км/годину')
            
        },
        addVolume : function (x){
            this.passangers +=x;
            console.log ( 'Потяг ' + this.name + ' підбирає '+ x + ' пасажирів і теперх у потязі ' + this.passangers + ' пасажирів ')
        }
    }
});