/*

    Задание 2.

    Напишите фунцию, которая изменяет цвет-фона и цвет текста, присваивая к новым цветам
    значение из this.color, this.background
    А так же добавляет элемент h1 с текстом "I know how binding works in JS"

    1.1 Ф-я принимает один аргумент,
    второй попадает в него через метод .call(obj)

    1.2 Ф-я не принимает никаких аргументов,
    а необходимые настройки полностью передаются через bind

    1.3 Ф-я принимает фразу для заголовка,
    обьект с настройками передаем через .apply();

*/
document.addEventListener("DOMContentLoaded", function () {
  // document.body.style.background = 'cyan';
    let colors = {
      background: 'red',
      color: 'white'
    }

    function myApply(  title ){
        document.body.style.background = this.background;
        let ss = document.createElement('h1');
        ss.innerHTML = title;
        ss.style.color = this.color;
        document.body.append( ss); 
    }
    myApply.bind(colors)
    myApply.apply(colors, ['Hello']);
        // var myBind = myApply.bind(colors);
        // myBind();
});
  
