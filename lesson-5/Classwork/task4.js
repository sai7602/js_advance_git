/*

  Задание "Шифр цезаря":

    https://uk.wikipedia.org/wiki/%D0%A8%D0%B8%D1%84%D1%80_%D0%A6%D0%B5%D0%B7%D0%B0%D1%80%D1%8F

    Написать функцию, которая будет принимать в себя слово и количество
    симовлов на которые нужно сделать сдвиг внутри.

    Написать функцию дешефратор которая вернет слово в изначальный вид.

    Сделать статичные функции используя bind и метод частичного
    вызова функции (каррирования), которая будет создавать и дешефровать
    слова с статично забитым шагом от одного до 5.


    Например:
      encryptCesar( 3, 'Word');
      encryptCesar1(...)
      ...
      encryptCesar5(...)

      decryptCesar1(3, 'Sdwq');
      decryptCesar1(...)
      ...
      decryptCesar5(...)

      "а".charCodeAt(); // 1072
      String.fromCharCode(189, 43, 190, 61) // ½+¾

*/
document.addEventListener('DOMContentLoaded', function(){

  function Encript(num, word){
    var arr1 = [];
    console.log('Origin: ' + word)
    for (i = 0; i < word.length; i++){
      var chart = word[i].charCodeAt()
      if (chart < 3000 ) {
        chart += num;
      } else {
        chart = chart -3000 + num;
      }
      arr1.push(String.fromCharCode(chart))
    }
    console.log('Encript: ' + arr1.join(''))
    Decript(num, arr1.join('') )
  }
  
  function Decript(num, word){
    var arr1 = [];
    console.log('Origin: ' + word)
    for (i = 0; i < word.length; i++){
      var chart = word[i].charCodeAt()
      if (chart < 3000 ) {
        chart -= num;
      } else {
        chart = chart -3000 - num;
      }
      arr1.push(String.fromCharCode(chart))
    }
    console.log('Decript: ' + arr1.join(''))

  }
  //Можливо знову не зрозумів завдання. Почав робиит bind але по синтаксису виходило майже те саме 
   var SimpleCarryingEncript = Encript.bind(null, 1);
  SimpleCarryingEncript("test", 3);
  Encript(3, 'абвгд');
})
