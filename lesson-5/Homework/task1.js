/*

  Задание:


    1. Написать конструктор объекта комментария который принимает 3 аргумента
    ( имя, текст сообщения, ссылка на аватаку);

    {
      name: '',
      text: '',
      avatarUrl: '...jpg'
      likes: 0
    }

      + Создать прототип, в котором будет содержаться ссылка на картинку по умлочанию
      + В прототипе должен быть метод который увеличивает счетик лайков

    var myComment1 = new Comment(...);

    2. Создать массив из 4х комментариев.
    var CommentsArray = [myComment1, myComment2...]

    3. Созадть функцию конструктор, которая принимает массив коментариев.
      И выводит каждый из них на страничку.

    <div id="CommentsFeed"></div>
*/
document.addEventListener("DOMContentLoaded", function () {
  let commentsFeed = document.getElementById('CommentsFeed');
  let myComment1 = new Comment('tuzik1', 'dvorterier1', 'input');
  let myComment2 = new Comment('tuzik2', 'dvorterier', 'idle');
  let myComment3 = new Comment('tuzik3', 'dvorterier', 'idle');
  let myComment4 = new Comment('tuzik4', 'dvorterier', 'idle');
  let arr = [myComment1, myComment2, myComment3, myComment4];
  let myAddComment = new AddComment(arr);
 
    function Comment(name, text, avatarUrl){
      this.name = name;
      this.text = text;
      this.avatarUrl = avatarUrl; 
      /* тут також не зрозумів, якщо я створюю конструктор то я повинен передати 3 параметри
        в тому числі і посилання на аватарку, що в цьому випадку дає вставка аватарки за 
        замовчуванням в прототип?
      */
      this.likes = 0;
    }
// задаю прототипи
  
  Comment.prototype = {
      avatarUrl: "test",
        likesAdd:  function(){
          this.likes ++;
      }
    }

  function AddComment(array){
    array.forEach(element => {
      //відображення коментарів з усіма полями
      for (var key in element) {
        var com = document.createElement('div');
        com.innerHTML = `${key} : ${element[key]}
         `;
        commentsFeed.append(com);
        //console.log('prop:', element, element[key] );
        }
// відображення коментарів тількт з вказаними полями. Звісно всі ці данні потрібно виводити в форму.
        // com.innerHTML = `
        // name: ${element.name}
        // text: ${element.text}
        // avatarUrl: ${element.avatarUrl}
        // `;
    });
  }
});