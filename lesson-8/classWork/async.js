/*

  Задание:

    Написать при помощи async-await скрипт, который:

    Получает список компаний:  http://www.json-generator.com/api/json/get/ceRHciXcVu?indent=2
    Перебирает, выводит табличку:

    # | Company  | Balance | Показать дату регистрации | Показать адресс |
    1.| CompName | 2000$   | button                    | button
    2.| CompName | 2000$   | 20/10/2019                | button
    3.| CompName | 2000$   | button                    | button
    4.| CompName | 2000$   | button                    | button

    Данные о дате регистрации и адресс скрывать при выводе и показывать при клике.

*/

let url = 'http://www.json-generator.com/api/json/get/ceRHciXcVu?indent=2';
  async function company (){
    const getCompany = await fetch(url);
    const companyJson = await getCompany.json();

    let node2 = document.getElementById('asyncAwait1');
    companyJson.forEach(seluse => {
        const addr1 = JSON.stringify(seluse.address)
        const adr = addr1.slice(1, addr1.length-1)
        console.log(`1 | ${seluse.company} | ${seluse.balance} | ${seluse.registered} | ${adr}`);
        let node = document.createElement('div');
        node.innerHTML = `<div class="asyncAwait" >
          <span>${seluse.company}</span>
          <span>${seluse.balance}</span>
          <button class="_dateBtn" value="Date">Date</button>
          <button class="_adrBtn" value="adress">Adress</button>
        </div>`
        node.querySelector('._dateBtn').addEventListener('click', (e) => {
          e.preventDefault();
          console.log (e.target)
        e.target.textContent =  seluse.registered;
        })

        node.querySelector('._adrBtn').addEventListener('click', (e) => {
          e.preventDefault();
          console.log (e.target)
        e.target.textContent =  adr;
        })
      node2.append(node);
  });
  }
        company()
