/*
    FETCH
*/

    var myHeaders = new Headers();
    myHeaders.append("Content-Type", "text/plain");
    myHeaders.append("Content-Length", 255);
    myHeaders.append("X-Custom-Header", "ProcessThisImmediately");

    function FetchDemo(){
        let url = 'http://localhost:3122/data';
        let options = {
        // Все опции:
        // https://developer.mozilla.org/en-US/docs/Web/API/WindowOrWorkerGlobalScope/fetch
        method: 'GET', // GET, PUT, DELETE etc.
        headers: {
            "Content-Type": "application/json"
        },
        /*
            https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers
            Сюда можно передать любые http headers как просто обьектом:
            {
            "Content-type": "application/x-www-form-urlencoded; charset=UTF-8"
            }
            Или через конструктор
    
            var myHeaders = new Headers();
                myHeaders.append("Content-Type", "text/plain");
                myHeaders.append("Content-Length", 255);
                myHeaders.append("X-Custom-Header", "ProcessThisImmediately");
    
            https://developer.mozilla.org/en-US/docs/Web/API/Headers/Headers
            https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers
        */
    
        // body: body, // Любые данные для вашего запроса
        // mode: 'same-origin',

        /*
            CORS: Cross-Origin Resourse Shairing
            Если просто: то в каком режиме кросс-доммености будет сделан запрос
            Если сложно: доки: https://developer.mozilla.org/en-US/docs/Web/HTTP/CORS
        */
        };
        
        const renderData = ( data ) => {
            data.map( item => {
                document.body.innerHTML += `<h2>${item.id}</h2>`;
            })
        }
        
        let x = fetch( url, options )
        .then( res => res.json() )
        .then( res => {
            return fetch(...)
        })
        .then( renderData );

    }

    document.addEventListener('DOMContentLoaded', () => {
        FetchDemo();
    });