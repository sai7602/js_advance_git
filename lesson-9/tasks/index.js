import input from "./default_bird";
import {Post} from "./ability";
import fly from "./move";

    class Bird {
        constructor(name, ability, move){
            this.defAbility = input.defAbility;
            this.ability = {ability, move};
            this.name = name;
        }
        ability1(){
            let finish = `${this.name}: ${this.defAbility.eat()}; ${this.defAbility.input()}; ${this.ability}`
            console.log(finish)
        }
    }
let Pigeon  = new Bird('Голуб', fly, Post)
console.log(Pigeon)