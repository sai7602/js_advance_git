/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./lesson-9/tasks/index.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./lesson-9/tasks/ability.js":
/*!***********************************!*\
  !*** ./lesson-9/tasks/ability.js ***!
  \***********************************/
/*! exports provided: Bearing, Eat, Hunt, Post, default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"Bearing\", function() { return Bearing; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"Eat\", function() { return Eat; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"Hunt\", function() { return Hunt; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"Post\", function() { return Post; });\nconst Bearing = () => {\r\n    console.log('Несе яйця')\r\n}\r\n\r\nconst Eat = () => {\r\n    console.log('Їсть');\r\n}\r\n\r\nconst Hunt = () => {\r\n    console.log('Охотиться');\r\n}\r\n\r\nconst Post = () => {\r\n    console.log('Носить пошту');\r\n}\r\n\r\n/* harmony default export */ __webpack_exports__[\"default\"] = (Bearing);\n\n//# sourceURL=webpack:///./lesson-9/tasks/ability.js?");

/***/ }),

/***/ "./lesson-9/tasks/default_bird.js":
/*!****************************************!*\
  !*** ./lesson-9/tasks/default_bird.js ***!
  \****************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _ability__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./ability */ \"./lesson-9/tasks/ability.js\");\n\r\n\r\nlet defAbility = {\r\n    defAbility: {input: _ability__WEBPACK_IMPORTED_MODULE_0__[\"default\"], eat: _ability__WEBPACK_IMPORTED_MODULE_0__[\"Eat\"]}\r\n  };\r\n/* harmony default export */ __webpack_exports__[\"default\"] = (defAbility);\r\n\r\n\n\n//# sourceURL=webpack:///./lesson-9/tasks/default_bird.js?");

/***/ }),

/***/ "./lesson-9/tasks/index.js":
/*!*********************************!*\
  !*** ./lesson-9/tasks/index.js ***!
  \*********************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _default_bird__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./default_bird */ \"./lesson-9/tasks/default_bird.js\");\n/* harmony import */ var _ability__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./ability */ \"./lesson-9/tasks/ability.js\");\n/* harmony import */ var _move__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./move */ \"./lesson-9/tasks/move.js\");\n\r\n\r\n\r\n\r\n    class Bird {\r\n        constructor(name, ability, move){\r\n            this.defAbility = _default_bird__WEBPACK_IMPORTED_MODULE_0__[\"default\"].defAbility;\r\n            this.ability = {ability, move};\r\n            this.name = name;\r\n        }\r\n        ability1(){\r\n            let finish = `${this.name}: ${this.defAbility.eat()}; ${this.defAbility.input()}; ${this.ability}`\r\n            console.log(finish)\r\n        }\r\n    }\r\nlet Pigeon  = new Bird('Голуб', _move__WEBPACK_IMPORTED_MODULE_2__[\"default\"], _ability__WEBPACK_IMPORTED_MODULE_1__[\"Post\"])\r\nconsole.log(Pigeon)\n\n//# sourceURL=webpack:///./lesson-9/tasks/index.js?");

/***/ }),

/***/ "./lesson-9/tasks/move.js":
/*!********************************!*\
  !*** ./lesson-9/tasks/move.js ***!
  \********************************/
/*! exports provided: Fly, Dive, Run, default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"Fly\", function() { return Fly; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"Dive\", function() { return Dive; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"Run\", function() { return Run; });\nconst Fly = () => {\r\n    return console.log('Літає')\r\n}\r\n\r\nconst Dive = () => {\r\n    console.log('Плаває');\r\n}\r\n\r\nconst Run = () => {\r\n    console.log('Бігає');\r\n}\r\n\r\n\r\n/* harmony default export */ __webpack_exports__[\"default\"] = (Fly);\r\n\n\n//# sourceURL=webpack:///./lesson-9/tasks/move.js?");

/***/ })

/******/ });