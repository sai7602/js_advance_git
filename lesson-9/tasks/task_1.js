/*

    Задание 1:
    Написать скрипт, который по клику на кнопку рандомит цвет и записывает его в localStorage
    После перезагрузки страницы, цвет должен сохранится.

*/
    function getRandom() {
        var num = Math.floor(Math.random() * 255);
        var converted = num.toString(16);
        if( converted.length === 1 ){
        converted = "0" + converted;
        }
        return converted ;
    }
    let btn = document.createElement('button')
    btn.textContent = "task1"
    btn.addEventListener('click', () => {
    let color =  "#" + getRandom() + getRandom() + getRandom(); 
    localStorage.setItem('color', JSON.stringify(color))
    console.log( JSON.parse( localStorage.getItem("color") ) );
    })
    document.body.append(btn);