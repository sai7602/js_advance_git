/*

    Задание 3:


    Написать класс Posts в котором есть данные:

    _id
    isActive,
    title,
    about,
    likes,
    created_at

    У класса должен быть метод addLike и render.

    Нужно сделать так чтобы:
    - После добавления поста, данные о нем записываются в localStorage.
    - После перезагрузки страницы, данные должны сохраниться.
    - Можно было прездагрузить данные в данный модуль: http://www.json-generator.com/api/json/get/cgCRXqNTtu?indent=2

*/

class Post {
    constructor(title, about, isActive){
        this._id = `f${(+new Date).toString(16)}`;
        this.about =  about;
        this.isActive = isActive;
        this.title = title;
        this.likes = 0;
        let date = new Date;
        let timeZoneHour = date.getTimezoneOffset()/60;
        let timeZoneMinutes = Math.abs(date.getTimezoneOffset()%60);
            if (timeZoneMinutes.toString().length === 1) {
                timeZoneMinutes = `0${timeZoneMinutes}`
            }
        if ((Math.abs(timeZoneHour)).toString().length === 1 ){
            if (timeZoneHour < 0 ){
                timeZoneHour = `-0${(-1)*date.getTimezoneOffset()/60}:${timeZoneMinutes}`
            } else {
                timeZoneHour = `0${(-1)*date.getTimezoneOffset()/60}:${timeZoneMinutes}`
            }
        } 
        this.created_at = `${date.getFullYear()}-${date.getMonth()+1}-${date.getDate()}T${date.toLocaleTimeString()} ${timeZoneHour}`
    }
    addLike(){
        this.likes ++;
    }
    render(){
        localStorage.setItem('post', JSON.stringify(this))
    }
}
    let title = `Test Title`;
    let about = `test About`;
    let post = new Post(title, about, true );
    console.log('post: ', post);
    post.addLike();
    post.addLike();
    post.addLike();
    post.render();
    