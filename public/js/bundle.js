/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./lesson-9/application/index.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./lesson-9/application/classes/class1.js":
/*!************************************************!*\
  !*** ./lesson-9/application/classes/class1.js ***!
  \************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\nclass test1{\n  //...\n}\n\n/* harmony default export */ __webpack_exports__[\"default\"] = (test1);\n\n\n//# sourceURL=webpack:///./lesson-9/application/classes/class1.js?");

/***/ }),

/***/ "./lesson-9/application/classes/class2.js":
/*!************************************************!*\
  !*** ./lesson-9/application/classes/class2.js ***!
  \************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\nclass test2{}\n\n/* harmony default export */ __webpack_exports__[\"default\"] = (test2);\n\n\n//# sourceURL=webpack:///./lesson-9/application/classes/class2.js?");

/***/ }),

/***/ "./lesson-9/application/classes/index.js":
/*!***********************************************!*\
  !*** ./lesson-9/application/classes/index.js ***!
  \***********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _class1__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./class1 */ \"./lesson-9/application/classes/class1.js\");\n/* harmony import */ var _class2__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./class2 */ \"./lesson-9/application/classes/class2.js\");\n\n\n\n/* harmony default export */ __webpack_exports__[\"default\"] = ({ test1: _class1__WEBPACK_IMPORTED_MODULE_0__[\"default\"], test2: _class2__WEBPACK_IMPORTED_MODULE_1__[\"default\"] });\n\n\n//# sourceURL=webpack:///./lesson-9/application/classes/index.js?");

/***/ }),

/***/ "./lesson-9/application/imports.js":
/*!*****************************************!*\
  !*** ./lesson-9/application/imports.js ***!
  \*****************************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _modules__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./modules */ \"./lesson-9/application/modules/index.js\");\n/* harmony import */ var _modules_combined__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./modules/combined */ \"./lesson-9/application/modules/combined.js\");\n  /*\n    import defaultExport from \"module-name\";\n    import * as name from \"module-name\";\n    import { export } from \"module-name\";\n    import { export as alias } from \"module-name\";\n    import { export1 , export2 } from \"module-name\";\n    import { export1 , export2 as alias2 } from \"module-name\";\n    import defaultExport, * as name from \"module-name\";\n    import \"module-name\";\n  */\n  /*\n    import defaultExport from \"module-name\";\n  */\n\n  // import Func from './modules/myFunction';\n  // Func();\n\n  // import DefaultVariavle, { KEY, TO} from './modules/';\n  // console.log( DefaultVariavle, KEY, TO );\n\n    \n    console.log( 'MyValue', _modules__WEBPACK_IMPORTED_MODULE_0__[\"KEY\"] );\n\n  // import Classes from './classes';\n  //\n  // console.log( Classes );\n\n   // 15kb\n  console.log( _modules_combined__WEBPACK_IMPORTED_MODULE_1__[\"User\"], _modules_combined__WEBPACK_IMPORTED_MODULE_1__[\"mObj\"], _modules_combined__WEBPACK_IMPORTED_MODULE_1__[\"mArray\"]);\n\n  let Butch = new _modules_combined__WEBPACK_IMPORTED_MODULE_1__[\"User\"]('Butch');\n  console.log( Butch );\n\n\n\n  // import * as Modules from './modules';\n\n  //\n  // export default \"Default export string\";\n\n\n//# sourceURL=webpack:///./lesson-9/application/imports.js?");

/***/ }),

/***/ "./lesson-9/application/index.js":
/*!***************************************!*\
  !*** ./lesson-9/application/index.js ***!
  \***************************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _imports__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./imports */ \"./lesson-9/application/imports.js\");\n/* harmony import */ var _classes___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./classes/ */ \"./lesson-9/application/classes/index.js\");\n/*\n\n  Модули в JS\n  https://developer.mozilla.org/ru/docs/Web/JavaScript/Reference/Statements/import\n\n  Так как для экспорта и импорта нету родной поддержки в\n  браузерах, то нам понадобится сборщик который это умеет делать\n\n  Выбор пал на вебпак\n\n  npm i\n  npm run cli\n\n  Затестим - в консоли наберем команду webpack\n*/\n\n// console.log('WEBPACK WORKING45567!')\n\n\n\n\n// Для экспорта и импорта обязательно нужно имя\n\nconsole.log( 'Classes', _classes___WEBPACK_IMPORTED_MODULE_1__[\"default\"] )\n\n// let { test1 } = Classes;\n// console.log('index module1', str);\n\n// let { prop1, prop2 } = x;\n// console.log( prop1, prop2 );\n\n// import SMSNG from './modules/1';\n\n// import { str, st2 } from './modules/1'; // 15 kb\n// console.log( str, st2 );\n// //\n// import someObj from './modules/2';\n// console.log(someObj);\n// // // // //\n// import { mArray, mObj, User } from './modules/3';\n// console.log( mArray, mObj, User);\n// //\n// const Butch = new User('Butch');\n// console.log(Butch);\n//\n// import $ from 'jquery';\n// $('html').html('<h1>Hello!</h1>');\n\n\n//# sourceURL=webpack:///./lesson-9/application/index.js?");

/***/ }),

/***/ "./lesson-9/application/modules/combined.js":
/*!**************************************************!*\
  !*** ./lesson-9/application/modules/combined.js ***!
  \**************************************************/
/*! exports provided: mArray, mObj, User, default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"mArray\", function() { return mArray; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"mObj\", function() { return mObj; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"User\", function() { return User; });\nconst mArray = ['1','2','3'];\nconst mObj = {\n  data: ['...']\n};\n\n class User {\n  constructor(name)  {\n    this.name = name;\n  }\n}\n\n\n/* harmony default export */ __webpack_exports__[\"default\"] = (User);\n\n\n//# sourceURL=webpack:///./lesson-9/application/modules/combined.js?");

/***/ }),

/***/ "./lesson-9/application/modules/index.js":
/*!***********************************************!*\
  !*** ./lesson-9/application/modules/index.js ***!
  \***********************************************/
/*! exports provided: KEY, TO, LEARNING, IS, DO, THINGS, default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"KEY\", function() { return KEY; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"TO\", function() { return TO; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"LEARNING\", function() { return LEARNING; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"IS\", function() { return IS; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"DO\", function() { return DO; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"THINGS\", function() { return THINGS; });\n/* harmony import */ var _manyExports__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./manyExports */ \"./lesson-9/application/modules/manyExports.js\");\n/* harmony import */ var _object__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./object */ \"./lesson-9/application/modules/object.js\");\n/* harmony import */ var _combined__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./combined */ \"./lesson-9/application/modules/combined.js\");\n/* harmony import */ var _myFunction__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./myFunction */ \"./lesson-9/application/modules/myFunction.js\");\n\n\n\n\n\nconst KEY = \"KEY\";\nconst TO = \"TO\";\nconst LEARNING = \"LEARNING\";\nconst IS = \"IS\";\nconst DO = \"DO\";\nconst THINGS = \"THINGS\";\n\n/* harmony default export */ __webpack_exports__[\"default\"] = ({\n  manyExports: _manyExports__WEBPACK_IMPORTED_MODULE_0__[\"default\"],\n  object: _object__WEBPACK_IMPORTED_MODULE_1__[\"default\"],\n  combined: _combined__WEBPACK_IMPORTED_MODULE_2__[\"default\"],\n  func: _myFunction__WEBPACK_IMPORTED_MODULE_3__[\"default\"]\n});\n\n\n//# sourceURL=webpack:///./lesson-9/application/modules/index.js?");

/***/ }),

/***/ "./lesson-9/application/modules/manyExports.js":
/*!*****************************************************!*\
  !*** ./lesson-9/application/modules/manyExports.js ***!
  \*****************************************************/
/*! exports provided: chorus1, chorus2, chorus3, chorus4, chorus5, chorus6, verse1, verse2, verse3, verse4, StairwayToHeaven, default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"chorus1\", function() { return chorus1; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"chorus2\", function() { return chorus2; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"chorus3\", function() { return chorus3; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"chorus4\", function() { return chorus4; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"chorus5\", function() { return chorus5; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"chorus6\", function() { return chorus6; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"verse1\", function() { return verse1; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"verse2\", function() { return verse2; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"verse3\", function() { return verse3; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"verse4\", function() { return verse4; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"StairwayToHeaven\", function() { return StairwayToHeaven; });\n\nconst chorus1 = \"There's a feeling I get\";\nconst chorus2 = \"When I look to the west\";\nconst chorus3 = \"And my spirit is crying for leaving\";\nconst chorus4 = \"In my thoughts I have seen\";\nconst chorus5 = \"Rings of smoke through the trees\";\nconst chorus6 = \"And the voices of those who standing looking\";\n\nconst verse1 = \"And if you listen very hard\";\nconst verse2 = \"The tune will come to you at last\";\nconst verse3 = \"To be a rock and not to roll\";\nconst verse4 = \"And she's buying the stairway to heaven\";\n\n\nconst StairwayToHeaven = ( type =  \"verse\" ) => {\n  if( type === \"verse\"){\n    return `\n      ${verse1}\n      ${verse2}\n      ${verse3}\n      ${verse4}\n    `;\n  } else {\n    return `\n      ${chorus1}\n      ${chorus2}\n      ${chorus3}\n      ${chorus4}\n      ${chorus5}\n      ${chorus6}\n    `\n  }\n}\n\n\n\n/* harmony default export */ __webpack_exports__[\"default\"] = (StairwayToHeaven);\n\n\n//# sourceURL=webpack:///./lesson-9/application/modules/manyExports.js?");

/***/ }),

/***/ "./lesson-9/application/modules/myFunction.js":
/*!****************************************************!*\
  !*** ./lesson-9/application/modules/myFunction.js ***!
  \****************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\nconst myFunction = () => {\n  console.log('My Super Function was exported and used!');\n}\n/* harmony default export */ __webpack_exports__[\"default\"] = (myFunction);\n\n\n//# sourceURL=webpack:///./lesson-9/application/modules/myFunction.js?");

/***/ }),

/***/ "./lesson-9/application/modules/object.js":
/*!************************************************!*\
  !*** ./lesson-9/application/modules/object.js ***!
  \************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\nlet myObj = {\n  name: 'Frodo',\n  age: '39'\n};\n\n/* harmony default export */ __webpack_exports__[\"default\"] = (myObj);\n\n\n//# sourceURL=webpack:///./lesson-9/application/modules/object.js?");

/***/ })

/******/ });